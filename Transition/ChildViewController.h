//
//  ChildViewController.h
//  Transition
//
//  Created by Soumya Mandi on 8/22/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FULLPdfView.h"
@interface ChildViewController : UIViewController {
    CGPDFPageRef pdfPage;
    FULLPdfView *pageView;
}
@property (assign, nonatomic) NSInteger index;
@property (assign, nonatomic) CGPDFPageRef pdfPage;
@property (strong, nonatomic) FULLPdfView *pageView;
@end
