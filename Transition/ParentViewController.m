//
//  ParentViewController.m
//  Transition
//
//  Created by Soumya Mandi on 8/22/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import "ParentViewController.h"

@interface ParentViewController ()

@end

@implementation ParentViewController
@synthesize pdfDoc,numberOfPages;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(CGPDFDocumentRef)pdfDocument
{
    NSLog(@"PDFView.pdfDocument()");
	if (pdfDoc == NULL)
	{
		CFURLRef pdfURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("city.pdf"), NULL, NULL);
		pdfDoc = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
		CFRelease(pdfURL);
	}
    return pdfDoc;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"size of ipad is (%.2f,%.2f)", self.view.frame.size.width, self.view.frame.size.height);
    //initialize pdf
    [self pdfDocument];
    size_t numPages = CGPDFDocumentGetNumberOfPages(pdfDoc);
    numberOfPages = numPages;
    NSLog(@"numPages is %d", numberOfPages);
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    ChildViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
//    [self.pageController didMoveToParentViewController:self];
    
    
}

- (ChildViewController *)viewControllerAtIndex:(NSUInteger)index
{
    NSLog(@"PVC.vCAI() ");
    if (pdfDoc == nil) {
        NSLog(@"pdfDoc is nil ;(");
    }
    else {
        NSLog(@"pdfDoc is good :)");
    }
    ChildViewController *childViewController = [[ChildViewController alloc] initWithNibName:nil bundle:nil];
    childViewController.index = index;
    
    CGPDFPageRef page = CGPDFDocumentGetPage(pdfDoc, index+1);
    if (page == nil) {
        NSLog(@"pdf's page is nil :(");
    }
    else {
        NSLog(@"pdf's page is good :)");
    }
    
    //set the pdf page
    [childViewController setPdfPage:page];
    
    return childViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSLog(@"PVC.pageVC vCBVC");
    NSUInteger index = [(ChildViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSLog(@"PVC.pageVC vCAVC");
    
    NSUInteger index = [(ChildViewController *)viewController index];
    
    index++;
    
    if (index == numberOfPages-1) {
        return nil;
        
    }
    
    return [self viewControllerAtIndex:index];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return numberOfPages;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}



@end
