//
//  FULLPdfView.m
//  FullPDF
//
//  Created by Soumya Mandi on 8/20/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import "FULLPdfView.h"

@implementation FULLPdfView

@synthesize pdfPage,myScale;
- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"initWithFrame");
    NSLog(@"Frame: %@", NSStringFromCGRect(frame));
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor redColor]];
    }
    
    return self;
}

// Create a new TiledPDFView with the desired frame and scale.
- (id)initWithFrame:(CGRect)frame scale:(CGFloat)scale
{
    NSLog(@"ZPDFV.initWithFrame()");
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}


-(void)drawInContext:(CGContextRef)context
{
	// Default is to do nothing.
	// PDF page drawing expects a Lower-Left coordinate system, so we flip the coordinate system
	// before we start drawing.
	CGContextTranslateCTM(context, 0.0, self.bounds.size.height);
	CGContextScaleCTM(context, 1.0, -1.0);
	
	// Grab the first PDF page
	//CGPDFPageRef page = CGPDFDocumentGetPage([self pdfPage], 1);
    CGPDFPageRef page = pdfPage;
	// We're about to modify the context CTM to draw the PDF page where we want it, so save the graphics state in case we want to do more drawing
	CGContextSaveGState(context);
	// CGPDFPageGetDrawingTransform provides an easy way to get the transform for a PDF page. It will scale down to fit, including any
	// base rotations necessary to display the PDF page correctly.
	CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFCropBox, self.bounds, 0, true);
	// And apply the transform.
	CGContextConcatCTM(context, pdfTransform);
	// Finally, we draw the page and restore the graphics state for further manipulations!
	CGContextDrawPDFPage(context, page);
	CGContextRestoreGState(context);
}

-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    NSLog(@"Inside drawRect method");
	[self drawInContext:UIGraphicsGetCurrentContext()];
}



- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    NSLog(@"VC.vFZISCV()");
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
