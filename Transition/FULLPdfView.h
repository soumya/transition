//
//  FULLPdfView.h
//  FullPDF
//
//  Created by Soumya Mandi on 8/20/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface FULLPdfView : UIView
{
    CGFloat myScale;
    CGPDFPageRef pdfPage;
}

@property (assign,nonatomic) CGPDFPageRef pdfPage;
@property (assign, nonatomic) CGFloat myScale;
- (id)initWithFrame:(CGRect)frame scale:(CGFloat)scale;
@end
