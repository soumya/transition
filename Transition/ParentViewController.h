//
//  ParentViewController.h
//  Transition
//
//  Created by Soumya Mandi on 8/22/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildViewController.h"
@interface ParentViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (assign, nonatomic) CGPDFDocumentRef pdfDoc;
@property (assign, nonatomic) NSInteger numberOfPages;
@end
