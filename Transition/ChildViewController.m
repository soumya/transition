//
//  ChildViewController.m
//  Transition
//
//  Created by Soumya Mandi on 8/22/13.
//  Copyright (c) 2013 Soumya Mandi. All rights reserved.
//

#import "ChildViewController.h"

@interface ChildViewController ()

@end

@implementation ChildViewController
@synthesize pdfPage,pageView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
      self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    NSLog(@"CVC.initWithNibName");

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"CVC.vDL()");
    // Do any additional setup after loading the view from its nib.
    self.pageView = [[FULLPdfView alloc] initWithFrame:CGRectMake(0,0, 768, 1004)];
    self.pageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    //self.pageView = [[FULLPdfView alloc] initWithFrame:self.view.bounds ];
    
    [self.pageView setPdfPage:pdfPage];
    if (self.pageView == nil) {
        NSLog(@"self.pageView is nil");
    }
    else {
        NSLog(@"self.pageView is not nil");
    }
    [self.pageView setPdfPage:pdfPage];

    NSLog(@"CVC.vDL()");
    if (self.pageView == nil) {
        NSLog(@"self.pageView is nil");
    }
    else {
        NSLog(@"self.pageView is not nil");
    }
    [self.pageView setPdfPage:pdfPage];
    //[self.view removeFromSuperview];
    
    [self.view addSubview:self.pageView];

    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
